//NOTE: Dependencies
const express = require('express');
/* 
!    Router
*        A router object is an isolated instance of middleware and routes. You can think of it as a “mini-application,” capable only of performing middleware and routing functions. Every Express application has a built-in app router.

*        A router behaves like middleware itself, so you can use it as an argument to app.use() or as the argument to another router’s use() method.
        https://expressjs.com/en/4x/api.html#router
*/
const router = express.Router();

//NOTE: Imported Modules
const userControllers = require('../controllers/userController');
const auth = require('../auth');

//Object destructuring from auth module
const { verify, verifyAdmin } = auth;

//NOTE: Routes
router.post('/', userControllers.registerUser);
router.get('/', userControllers.getAllUsers);
router.post('/login', userControllers.loginUser);
//get user details
router.get('/getUserDetails', verify, userControllers.getUserDetails);
router.get('/checkEmailExists', userControllers.checkEmailExists);

router.put('/updateUserDetails', verify, userControllers.updateUserDetails);

router.put(
  '/updateUserAdmin/:id',
  verify,
  verifyAdmin,
  userControllers.upateAdmin
);

router.post('/enroll', verify, userControllers.enroll);

router.get('/getEnrollments', verify, userControllers.getEnrollments);

module.exports = router;
