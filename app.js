//NOTE: Dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

//NOTE: Server
const app = express();
const port = 4000;

//NOTE: Database Connection
mongoose.connect(
  'mongodb+srv://admin:admin@wdc028-course-booking.j89li.mongodb.net/course-booking?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error'));
db.once('open', () => console.log('Successfully connected to MongoDB'));

//NOTE: Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//NOTE: Group Routing
const userRoutes = require('./routes/userRoutes');
//app.use([path,] callback [, callback...]) - (https://expressjs.com/en/4x/api.html#app.use)
app.use('/users', userRoutes);

const courseRoutes = require('./routes/courseRoutes');
app.use('/courses', courseRoutes);

//NOTE: Port listener
app.listen(port, () => console.log(`Server is listening in port: ${port}`));
/* 
Activity 1:

    >> Create a User model out of the rough sketch made for our models.
    >> Follow the sketch and add the fields and the types.
        Note: All fields are required except for isAdmin.
        Note: isAdmin, 
              Date, 
              status = "Enrolled" 
              has a default value.
*/

/* 
Activity 2:

    >> Create a new route and controller which will allow us to add/create a new Course document.
        -endpoint: '/'

        >> In courseRoutes.js:
        -Import express and save it in a variable called express.
        -Save the Router() method of express in a variable called router.
        -Import your courseControllers from your courseControllers file.
        -Create your route for course creation.

        >> Go back to your courseControllers.js file:
        -Import your Course model in the courseController.js file.
        -Add a new controller called addCourse which will allow us to add a course: 
        -Create a new course document out of your Course model.
        -Save your new course document.
            -Then send the result to the client.
            -Catch the error and send it to our client.

        >> Back in courseRoutes.js: 
        -Add the addCourse controller in your route.

    >> Create a new route and controller which will allow us to get all Course documents.
        -endpoint: '/'

        >> In courseRoutes.js:
        -Create a route to get all course documents.

        >> Go back to your courseControllers.js file:
        -Add a controller called getAllCourses which will allow us to find all course documents.
        -use the find() method of our Course model to find our documents.
            -Then send the result to the client.
            -Catch the error and send it to our client.

        >> Back in courseRoutes.js:
        -Add the getAllCourses controller in your route.


        >> Remember to:
        -import your courseRoutes in index.js
        -Add middleware to group your courseRoutes under '/courses'

    >> Test the routes in Postman
*/

/* 
Activity 3:

    Create 2 new routes and controllers:

    Course:
        1. Create a route which will be able to retrieve the details of a single Course.
            -endpoint: '/getSingleCourse/:id'
            -Create a new controller called getSingleCourse which is able to find a single course by its id.
                -Pass the id of the course through the url params.
                -Then send the result to our client.
                -Catch the error and send it to our client.
                -All users may use this route. This route will not use verify.

    User:
        2. Create a route which will be able to check if an input email already exists in our database.
            -endpoint: /checkEmailExists
            -Create a new controller called checkEmailExists which is able to find a single user by its email.
                -The request for this route would need a request body. It is in the request body where you can add the email.
                -Then send the result to our client.
                -Catch the error and send it to our client.
                -All users may use this route. This route will not use verify.
                
                Stretch Goal:
                    -Inside then(), check the result:
                    -IF null was returned as result, send a message to the client: "Email is available"
                    -Else, send a message to the client: "Email is already registered!"

*/

/* 
Activity 4:

    >> Create 3 new routes and controllers:

    1. Create a route which will be able to archive a Course document. (make a course inactive) 
        -endpoint: '/archive/:id'

        >> Create a controller which will be able to update a Course's isActive property to false.
            -the selected course's id may be passed through URL params.
            -use a query from our Course model which will allow us to find the document by its id and apply our updates.
            -Then send the updated Course's details to our client.
            -Catch the error and send it to our client.
            -Only an admin can access this route.
            -No need to pass a request body. You can directly update your isActive property to false.

    2. Create a route which will be able to activate a Course document.
        -endpoint: '/activate/:id' (make a course active)
        
        >> Create a controller which will be able to update a Course's isActive property to true.
            -the selected course's id may be passed through URL params.
            -use a query from our Course model which will allow us to find the document by its id and apply our updates.
            -Then send the updated Course's details to our client.
            -Catch the error and send it to our client.
            -Only an admin can access this route.
            -No need to pass a request body. You can directly update your isActive property to true.

    3. Create a route which will be able to retrieve all of Active course documents.
        -endpoint: '/getActiveCourses'
        
        >> Create a new controller called getActiveCourses to find all Course documents which active properties are true.
            -Then send the result to our client.
            -Catch the error and send it to our client.
            -All users may use this route. No need for verify.
*/
