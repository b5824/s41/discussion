/* 
    >> In courseRoutes.js:
    -Import express and save it in a variable called express.
    -Save the Router() method of express in a variable called router.
    -Import your courseControllers from your courseControllers file.
    -Create your route for course creation.

     >> Go back to your courseControllers.js file:
        -Import your Course model in the courseController.js file.
        -Add a new controller called addCourse which will allow us to add a course: 
        -Create a new course document out of your Course model.
        -Save your new course document.
            -Then send the result to the client.
            -Catch the error and send it to our client.
*/
const Course = require('../models/Course');

//! Redacted code.
// const addCourse = (req, res) => {
//check if the course exists in the database
//throw error if it does
//save in the db if it doesn't

////Always returns empty object if the name doesn't exist
// Course.findOne({ name: req.body.name })
//     .then((result) => {
//         if (result.name === req.body.name) {
//             return res.send('Course name already exists!');
//         } else {
//             let body = req.body;
//             const newCourse = new Course({
//                 name: body.name,
//                 description: body.description,
//                 price: body.price,
//             });
//             newCourse
//                 .save()
//                 .then((result) => res.send(result))
//                 .catch((err) => res.send(err));
//         }
//     })
//     .catch((err) => res.send(err));

////This code allows duplicates
// let body = req.body;
// const newCourse = new Course({
//     name: body.name,
//     description: body.description,
//     price: body.price,
// });
// newCourse
//     .save()
//     .then((result) => res.send(result))
//     .catch((err) => res.send(err));
// };

const addCourse = (req, res) => {
  //countDocuments - https://mongoosejs.com/docs/api.html#model_Model.countDocuments
  //countDocuments-stackOverflow link (https://stackoverflow.com/questions/10811887/how-to-get-all-count-of-mongoose-model)
  Course.countDocuments({ name: req.body.name }, (err, count) => {
    if (count === 0) {
      let body = req.body;
      const newCourse = new Course({
        name: body.name,
        description: body.description,
        price: body.price,
      });
      newCourse
        .save()
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
    } else {
      return res.send('Course name already exists!');
    }
  });
};

let getAllCourses = (req, res) => {
  Course.find({})
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

let getSingleCourse = (req, res) => {
  Course.findById({ _id: req.params.id })
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

//UPDATING A COURSE
let updateCourse = (req, res) => {
  let updates = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  };

  Course.findByIdAndUpdate(req.params.id, updates, { new: true })
    .then((upCourse) => res.send(upCourse))
    .catch((error) => res.send(error));
};

let archiveCourse = (req, res) => {
  let archive = {
    isActive: false,
  };

  //What do I want to change > How do I want to change it > Update it instantly
  Course.findByIdAndUpdate(req.params.id, archive, { new: true })
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

let activateCourse = (req, res) => {
  let archive = {
    isActive: true,
  };

  Course.findByIdAndUpdate(req.params.id, archive, { new: true })
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

let getActiveCourse = (req, res) => {
  Course.find({ isActive: true })
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

//NOTE: FIND COURSES BY NAME
const findCoursesByName = (req, res) => {
  console.log(req.body);

  Course.find({ name: { $regex: req.body.name, $options: '$i' } }).then(
    (result) => {
      if (result.length === 0) {
        return res.send('No courses found');
      } else {
        return res.send(result);
      }
    }
  );
};

module.exports = {
  addCourse,
  getAllCourses,
  getSingleCourse,
  updateCourse,
  archiveCourse,
  activateCourse,
  getActiveCourse,
  findCoursesByName,
};
